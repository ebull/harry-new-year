# Le grand livre des points

Ce repository contient le code de l'application de comptage des points des
maisons utilisé pour le nouvel an 2017/2018 spécial Harry Potter d'Ebullition.


Ce code n'est plus maintenu et est utilisable librement.

## Utilisation

Cette application peut être lancée dans des containers grâce à docker et
docker-compose. Il suffit de lancer la commande suivant dans ce dossier

	sudo docker-compose up

Cela va lancer deux containers: 1 contenant redis qui s'occupe du stockage des
données, et l'autrequi contient le server.

Une fois cette commande lancée, l'application est accessible à l'addresse `http://localhost`.