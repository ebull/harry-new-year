import atexit
import logging
import secrets

from flask import render_template, Flask, Response
from flask_socketio import SocketIO, emit
from flask_redis import FlaskRedis

redis_thread = None


# houses names
HOUSES = [
    'gryffondor',
    'serpentard',
    'poufsouffle',
    'serdaigle'
]

HAPPY_HOUR_POINTS = 1000 # number of points the set an happy hour on
HAPPY_HOUR_KEY = '{}-last-happy-hour' # format of the last happy hour key name

# minimal and maximal amounts of points allowed
MAX_POINTS = 200
MIN_POINTS = -200

# minimal and maximal amounts of points that does not trigger a cooldown
MAX_POINTS_WITHOUT_COOLDOWN = 4
MIN_POINTS_WITHOUT_COOLDOWN = -4

SCOREBOARD_NAME = 'scoreset'
COOLDOWN_KEY_NAME = '{}-{}-cooldown-value'

# value of the buttons on the web interface
BUTTONS_VALUES = [-1, -5, -10, -20, 1, 5, 10, 20, 50, 100]


def cooldown_handler(message):
    """Callback for redis key expiration event

    When a cooldown key is expired and deleted in redis, it means the cooldown
    is ended. This handler will send a socket.io event to notify the clients.
    
    Args:
        message: the redis event message
    """
    data = message['data'].split('-')
    response = {
        'house': data[0],
        'sign': data[1]
    }
    socketio.emit('cooldown ended', response, json=True)


def create_app():
    """Create the server and redis connection

    Return:
        the flask application, the socketio server and the redis store
    """
    app = Flask(__name__)
    app.config['SECRET_KEY'] = secrets.token_hex(24)
    app.config['REDIS_URL'] = "redis://redis/0"

    socketio = SocketIO(app)

    redis_store = FlaskRedis(app, decode_responses=True)
    redis_store.config_set('notify-keyspace-events', 'KEA')

    def start_redis_thread():
        """Start the redis subscription polling thread

        This function will subscribe to the redis events channel and create a
        new thread to poll these events.
        """
        sub = redis_store.pubsub()
        sub.subscribe(**{'__keyevent@0__:expired': cooldown_handler})
        global redis_thread
        redis_thread = sub.run_in_thread(sleep_time=0.001)

    def stop_redis_thread():
        """Stop the redis subscription polling thread"""
        redis_thread.stop()

    start_redis_thread()

    atexit.register(stop_redis_thread)

    return app, socketio, redis_store


app, socketio, redis_store = create_app()

# get gunicorn logger
gunicorn_error_logger = logging.getLogger('gunicorn.error')
app.logger.handlers.extend(gunicorn_error_logger.handlers)
app.logger.setLevel(logging.INFO)


def update_points(house, points):
    """Update the points of a house

    This function agives points to a house, checks if an happy hour is
    happening and updates the cooldowns accordingly. It emits appy hour
    socketio events if needed.

    Args:
        house   the house to give points to
        points  the amount of points to add
    
    Return:
        the updated scoreboard
    """
    points = int(points)
    if check_cooldown(house, points):
        return get_scoreboard(with_cooldowns=False)

    if points <= MAX_POINTS and points >= MIN_POINTS:
        update_cooldown(house, points)
        app.logger.info('{:<3} points added for {}'.format(points, house))
        new_points = int(redis_store.zincrby(SCOREBOARD_NAME, house, points))

        last_happy_hour = redis_store.get(HAPPY_HOUR_KEY.format(house))
        if last_happy_hour is None:
            last_happy_hour = 0
        else:
            last_happy_hour = int(last_happy_hour)

        if new_points // HAPPY_HOUR_POINTS > last_happy_hour:
            app.logger.info('Happy hour for {}'.format(house))
            redis_store.set(HAPPY_HOUR_KEY.format(house), new_points // HAPPY_HOUR_POINTS)
            emit('happy hour', {'house': house}, broadcast=True, json=True)

    return get_scoreboard(with_cooldowns=False)


def check_cooldown(house, points):
    """Check if a cooldown exists for a house

    This function checks if a certain amount of points can be given to a house
    according to the cooldowns.

    Args:
        house   the house to check
        points  the amount of points to check

    Return:
        False if there's no cooldown in place, so the points can be given to
        the house, True otherwise
    """
    key = COOLDOWN_KEY_NAME.format(house, 'pos' if points > 0 else 'neg')
    previous = redis_store.get(key.format(house))
    if previous is None:
        return False
    else:
        return (points > int(previous) and points > 0) or
               (points < int(previous) and points < 0)


def update_cooldown(house, points):
    """Update the cooldwon for a house

    Update the cooldown time and value of a house according the an amount of
    points. 

    The cooldowns are stored in two keys in redis: one for the positive
    cooldown and one for the negative. The value of the key is the maximum 
    amount of point before the limitation of the cooldown. The key has an
    expiration time which defines the time the cooldown is applied.

    This function emits a cooldown update socketio event to the clients.

    Args:
        house   the house to which the cooldown if applied
        points  the amount of points
    """
    if not (points <= MAX_POINTS_WITHOUT_COOLDOWN and points >= MIN_POINTS_WITHOUT_COOLDOWN):
        key = COOLDOWN_KEY_NAME.format(house, 'pos' if points > 0 else 'neg')

        crt_time = redis_store.ttl(key)
        time = int((abs(points) / 10) ** 1.6 * 15)
        if crt_time is not None and int(crt_time) > 0:
            time = time + int(crt_time)

        app.logger.info('{:<3} seconds cooldown for {}'.format(time, house))
        pipe = redis_store.pipeline()
        pipe.set(key, points)
        pipe.expire(key, time)
        pipe.ttl(key)
        result = pipe.execute()

        update_message = {
            'house': house,
            'cooldown_min_value': points,
            'cooldown_time': result[2]
        }
        emit('cooldown updated', update_message, broadcast=True, json=True)


def get_scoreboard(with_cooldowns=True):
    """Get the current scoreboard

    Args:
        with_cooldowns  get the current cooldowns keys (default: True)

    Return:
        the scoreboard sorted by the number of points descending
    """
    scoreboard = redis_store.zrevrange(SCOREBOARD_NAME, 0, -1, withscores=True, score_cast_func=int)
    response = {
        'scoreboard': [{'house': house_score[0],
                        'points': int(house_score[1]),
                        'rank': rank + 1} for rank, house_score in enumerate(scoreboard)]}

    if not with_cooldowns:
        return response

    response['cooldowns'] = []
    for house in HOUSES:
        house_cooldown = {'house': house}
        pipe = redis_store.pipeline()
        pipe.get(COOLDOWN_KEY_NAME.format(house, 'neg'))
        pipe.get(COOLDOWN_KEY_NAME.format(house, 'pos'))
        result = pipe.execute()

        if result[0] is None:
            house_cooldown['cooldown_neg_value'] = None
        else:
            house_cooldown['cooldown_neg_value'] = int(result[0])

        if result[1] is None:
            house_cooldown['cooldown_pos_value'] = None
        else:
            house_cooldown['cooldown_pos_value'] = int(result[1])

        response['cooldowns'].append(house_cooldown)

    return response


@app.route('/')
def hello():
    """The '/' route handler"""
    classes = ['bg-danger', 'bg-success', 'bg-warning', 'bg-primary']
    tpl_houses = [house for house in zip(classes, HOUSES)]
    return render_template('index.html', houses=tpl_houses, values=BUTTONS_VALUES)

@app.route('/help')
def help():
    """The '/help' route handler"""

    return render_template('help.html')


@app.route('/data.txt')
def data():
    """The '/data.txt' route handler

    This route returns a csv formatted file with the current scoreboard with
    the last happy hour value.
    """

    scoreboard = get_scoreboard()
    content = 'rank\thouse\tpoints\tlast_happy_hour\n'

    for score in scoreboard['scoreboard']:
        last_happy_hour = redis_store.get(HAPPY_HOUR_KEY.format(score['house']))
        if last_happy_hour is None:
            last_happy_hour = 0
        else:
            last_happy_hour = int(last_happy_hour)
        content = content + '{}\t{}\t{}\t{}\n'.format(score['rank'], score['house'], score['points'], last_happy_hour)

    return Response(content, mimetype='text/plain')


@socketio.on('connect')
def on_connect():
    """The 'connect' event handler"""
    app.logger.info('New connection')
    emit('scoreboard', get_scoreboard(), json=True)


@socketio.on('update points')
def on_points_update(data):
    """The 'update points' event handler"""
    response = update_points(data['house'], data['points'])
    emit('points updated', response, broadcast=True, json=True)


if __name__ == '__main__':
    socketio.run(app)
