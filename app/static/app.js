var socket = io();

function addPoints(points, house) {
    socket.emit('update points', {'points': points, 'house': house});
}

function toggleNegative(house) {
    btns = $('.' + house + '-btn');
    btns.each(function (idx, btn) {
        btn = $(btn);
        btn.toggleClass('d-none');
    });
}
function updateButtons(house, value) {
    if (value === null)
        return;

    btns = $('.' + house + '-btn');
    btns.each(function (idx, btn) {
        btn = $(btn);
        if (value > 0 && btn.data('value') >= value) {
            $(btn).addClass('disabled')
        }else if (value < 0 && btn.data('value') <= value) {
            $(btn).addClass('disabled')
        }
    });
}

function updateScoreboard(scoreboard) {
    for (i = 0; i < scoreboard.length; i++) {
        $('#' + scoreboard[i]['house'] + '-points').text(scoreboard[i]['points']);
        $('#' + scoreboard[i]['house'] + '-rank').text(scoreboard[i]['rank']);
    }
}

socket.on('points updated', function (data) {
    console.log('POINTS UPDATE', data);
    updateScoreboard(data['scoreboard']);
});

socket.on('scoreboard', function (data) {
    console.log('NEW SCOREBOARD', data);

    updateScoreboard(data['scoreboard']);

    cooldowns = data['cooldowns'];
    for (i = 0; i < cooldowns.length; i++){
        updateButtons(cooldowns[i]['house'], cooldowns[i]['cooldown_pos_value']);
        updateButtons(cooldowns[i]['house'], cooldowns[i]['cooldown_neg_value'])
    }
});

socket.on('cooldown updated', function (data) {
    console.log('COOLDOWN UPDATE', data);

    updateButtons(data['house'], data['cooldown_min_value'])
});

socket.on('cooldown ended', function (data) {
    console.log('COOLDOWN END', data);
    btns = $('.' + data['house'] + '-btn.' + data['sign'] + '-btn');
    btns.each(function (idx, btn) {
        $(btn).removeClass('disabled')
    });
});

socket.on('happy hour', function (data) {
    console.log('HAPPY HOUR', data);

    alert('La maison ' + data['house'] + ' gagne un happy hour!');
});